# container
FROM alpine:3.15.0

RUN apk update && \
    apk add texlive-full biber gnuplot py3-pygments && \
    rm -rf /var/lib/apt/lists/*

ADD pdfconverter.sh usr/bin/pdfconverter

RUN ["chmod", "+x", "usr/bin/pdfconverter"]

ENTRYPOINT ["/usr/bin/pdfconverter"]
