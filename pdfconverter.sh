#!/bin/bash

set -eax

texfot pdflatex --interaction nonstopmode "${TEX_ARGS}" "$@" | grep -v "Package biblatex Warning:" | grep -v "LaTeX Warning: Citation" | grep -v "LaTeX Warning: Reference"

# Optionally run Biber, BibTex and Glossaries
[[ "${ENABLE_BIBER}" == "true" ]] && biber "$@"
[[ "${ENABLE_BIBTEX}" == "true" ]] && bibtex "$@"
[[ "${ENABLE_GLOSSARIES}" == "true" ]] && makeglossaries "$@"

texfot pdflatex --interaction nonstopmode "${TEX_ARGS}" "$@"
texfot pdflatex --interaction nonstopmode "${TEX_ARGS}" "$@"

# Clean up temporary files
[[ "${ENABLE_CLEANUP}" == "true" ]] && rm -f *.{aux,log,out,bbl,blg,acn,acr,alg,run.xml,toc,lot,lof,ist,idx,glo,glg,gls,bcf}

exit 0
